public interface LibraryService {

    Book addBook(Library library, Book book);

    Book deleteBook (Library library, Book book);

    Book searchBook(Library library, Book book);

}
